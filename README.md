# TestVST
A simple test VST that people can expand on with RackAFX and Ableton Live

Download RackAFX from here: http://www.willpirkle.com/rackafx/downloads/

To build:

Open the TestVST.prj in RackAFX and set to compiler settings in the Preferences.

This VST was built with Visual Studio Community 2017.

The plugin is 64-bit so you will need a 64-bit version of Live 9 or higher supporting VST 2.x

![Prefereces](plugins.png)

To load the plugin create a "Steinberg" directory and put the VST in it.

Next open Live go to Options->Preferences->File Folder. And make sure custom VST folder is "on"

Next Browse for the you newly created Steinberg directory and rescan.

You should see the Test VST ith the Plugins to the left.


